Rails.application.routes.draw do
  root to: 'tickets#index'
  resources :tickets, only: %i[index show] do
    resources :comments, only: %i[create new]
  end
end
