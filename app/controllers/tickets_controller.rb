# frozen_string_literal: true

class TicketsController < ApplicationController
  def index
    @tickets = zendesk_client.tickets
  end

  def show
    @ticket = zendesk_client.tickets.find!(id: params[:id], include: :comments)
  end

  def create
    redirect_to ticket_path(params[:id])
  end
end
