# frozen_string_literal: true

class ApplicationController < ActionController::Base
  private

  def zendesk_client
    $ZENDESK_CLIENT
  end
end
