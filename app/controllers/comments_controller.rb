# frozen_string_literal: true

class CommentsController < ApplicationController
  def create
    upload_to_zendesk(params[:body], params[:attachments])
    redirect_to ticket_path(params[:ticket_id])
  end

  def new; end

  private

  def upload_to_zendesk(comment_body, files)
    ticket.comment = { value: comment_body }
    files.each do |file|
      ticket.comment.uploads << file
    end
    ticket.save
  end

  def ticket
    @ticket ||= zendesk_client.tickets.find!(id: params[:ticket_id])
  end
end
